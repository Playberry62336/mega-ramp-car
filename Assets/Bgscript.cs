﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bgscript : MonoBehaviour
{
    public Slider slider;

    public static Bgscript BgInstance;

    private void Awake()
    {
        if(BgInstance!=null && BgInstance != this)
        {
            Destroy(this.gameObject);
            DontDestroyOnLoad(this);

        }
    }
    private void Update()
    {
        gameObject.GetComponent<AudioSource>().volume = slider.value;
    }
}
