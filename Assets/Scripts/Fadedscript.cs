﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fadedscript : MonoBehaviour
{
    public bool Fade;
    public float Duation;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
 
    }
    public void DoFade()
    {
        var canvaGroup =gameObject.GetComponent<CanvasGroup>();
        StartCoroutine(Faded(canvaGroup, canvaGroup.alpha, Fade ? 1 : 0));

        Fade = !Fade;
    }
    public IEnumerator Faded(CanvasGroup canvasGroup,float start,float end)
    {
        float Counter = 0f;
        while(Counter<Duation)
        {
            Counter += Time.deltaTime;
            canvasGroup.alpha = Mathf.Lerp(start, end, Counter / Duation);
        }

        yield return null;
    }
}
