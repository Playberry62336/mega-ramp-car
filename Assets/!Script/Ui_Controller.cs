﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Ui_Controller : MonoBehaviour
{
    public GameObject information_Panel;
    public GameObject Splash_Panel;

    private void Awake()
    {
        Splash_Panel.SetActive(true);
        StartCoroutine("Delay");
    }
    IEnumerator Delay()
    {
      
        yield return new WaitForSeconds(5f);
        StartCoroutine("Delay1");
        
        
    }
    private IEnumerator Delay1()
    {
        yield return new WaitForSeconds(5f);
        information_Panel.SetActive(true);
        Splash_Panel.SetActive(false);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void OnClickSkip()
    {
        SceneManager.LoadScene(1);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
