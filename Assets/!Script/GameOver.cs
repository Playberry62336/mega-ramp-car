﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    public Game_Manager GM;

    public float Distance;
    // Start is called before the first frame update
    void Start()
    {
        Distance = gameObject.GetComponent<TriggerScript>().totalDistance;
    }

    // Update is called once per frame
    void Update()
    {
      
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("GameOver"))
        {
            Distance = gameObject.GetComponent<TriggerScript>().totalDistance;
            gameObject.GetComponent<TriggerScript>().Runcorutine(Distance);
            GM.UIM.Rekt_Panel.SetActive(true);
            StartCoroutine("DelayInRestart");
        }
    }
    IEnumerator DelayInRestart()
    {
        yield return new WaitForSeconds(5f);
        SceneManager.LoadScene(0);
    }
}
