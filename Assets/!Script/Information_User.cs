﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Information_User : MonoBehaviour
{
    public string eth_address;
    public string phantom_address;
    public string discord_Usernmae;
    public string email_address;
    public string Voucher;

    public InputField eth;
    public InputField phantom;
    public InputField discord;
    public InputField email;
    public InputField voucher;



    // Start is called before the first frame update
    void Start()
    {
        
        // StartCoroutine(postRequest("http:///www.yoururl.com"));
    }
    public void OnEndWritting()
    {
        eth_address = eth.text;

        Debug.Log(eth_address);
        phantom_address = phantom.text;

        Debug.Log(phantom_address);
        discord_Usernmae = discord.text;

        Debug.Log(discord);
        email_address = email.text;
        PlayerPrefs.SetString("Dicord", discord_Usernmae);
        PlayerPrefs.Save();

        Debug.Log(email_address);
        Voucher = voucher.text;

        Debug.Log(Voucher);

        StartCoroutine(Upload());

    }
    // Update is called once per frame
    void Update()
    {

    }
    IEnumerator Upload()
    {
        WWWForm form = new WWWForm();
        form.AddField("etherAddress", eth_address);
        form.AddField("phantomAddress", phantom_address);
        form.AddField("discordUserName", discord_Usernmae);

        PlayerPrefs.SetString("Discord", discord_Usernmae);
        PlayerPrefs.Save();

        form.AddField("email", email_address);
        form.AddField("score", 0);

        using (UnityWebRequest www = UnityWebRequest.Post("https://rank.rektracoonsgame.com/register", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log("Form upload complete!");
                SceneManager.LoadScene(1);

            }
        }
    }



    //    string abc = "aaa";
    //    var json = "{\"etherAddress\":"+abc+",\"phantomAddress\":\"abc\",\"discordUserName\":\"abc\",\"email\":\"abc\",\"score\":\"abc\"}";

    //    var uwr = new UnityWebRequest("http://206.189.206.42:3000/register",  "POST");
    //    byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(json);
    //    uwr.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend);
    //    uwr.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
    //    uwr.SetRequestHeader("Content-Type", "application/json");

    //    //Send the request then wait here until it returns
    //    yield return uwr.SendWebRequest();

    //    if (uwr.isNetworkError)
    //    {
    //        Debug.Log("Error While Sending: " + uwr.error);
    //    }
    //    else
    //    {
    //        Debug.Log("Received: " + uwr.downloadHandler.text);
    //    }
    //}

}