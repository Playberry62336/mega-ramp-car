﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using SWS;

public class TriggerScript : MonoBehaviour
{
    public Game_Manager GM;
    public RuntimeAnimatorController anim1;
    public GameObject Solana;
    public GameObject Etherium;
    public float solana;
    public float etherium;



    public string Discord;

    public Transform other;


    private Vector3 lastPosition;
    public float totalDistance;

    // Start is called before the first frame update
    void Start()
    {
        lastPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.transform.localPosition.z < lastPosition.z)
        {
            float distance = Vector3.Distance(lastPosition, transform.position);
            totalDistance += distance;
           
            totalDistance = Mathf.Round(totalDistance * 100f) / 100f;
  
            GM.UIM.distance_Cover.text = totalDistance.ToString();
            lastPosition = transform.position;
        }


       
    }
    public void Runcorutine( float Distance)
    {
        StartCoroutine(Upload(Distance));
    }

    IEnumerator Upload(float Distance)
    {
        WWWForm form = new WWWForm();


        Discord = PlayerPrefs.GetString("Discord");

        form.AddField("discordUserName", Discord);



        form.AddField("score", Distance.ToString());

        using (UnityWebRequest www = UnityWebRequest.Post("https://rank.rektracoonsgame.com/findAndUpdate", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log("Form upload complete!");
                SceneManager.LoadScene(1);

            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Trigger 1"))
        {
            for(int i=0;i<GM.Runners.Length;i++)
            {
                GM.Runners[i].GetComponent<splineMove>().enabled = true;
                GM.Runners[i].GetComponent<Animator>().runtimeAnimatorController = anim1;
                //GM.Runners[i].GetComponent<Animator>().enabled = true;
               
            }
           
        }

        if (other.gameObject.CompareTag("Trigger 2"))
        {
            for (int i = 0; i < GM.Runners2.Length; i++)
            {
                GM.Runners2[i].GetComponent<splineMove>().enabled = true;
                GM.Runners[i].GetComponent<Animator>().runtimeAnimatorController = anim1;
            }

        }

        if (other.gameObject.CompareTag("Trigger 3"))
        {
            for (int i = 0; i < GM.Runners3.Length; i++)
            {
                GM.Runners3[i].GetComponent<splineMove>().enabled = true;
                GM.Runners[i].GetComponent<Animator>().runtimeAnimatorController = anim1;
            }

        }

        if (other.gameObject.CompareTag("Trigger 4"))
        {
            for (int i = 0; i < GM.Runners4.Length; i++)
            {
                GM.Runners4[i].GetComponent<splineMove>().enabled = true;
                GM.Runners[i].GetComponent<Animator>().runtimeAnimatorController = anim1;
            }

        }

        if (other.gameObject.CompareTag("Trigger 5"))
        {
            for (int i = 0; i < GM.Runners5.Length; i++)
            {
                GM.Runners5[i].GetComponent<splineMove>().enabled = true;
                GM.Runners[i].GetComponent<Animator>().runtimeAnimatorController = anim1;
            }

        }

        if (other.gameObject.CompareTag("Trigger 6"))
        {
            for (int i = 0; i < GM.Runners6.Length; i++)
            {
                GM.Runners6[i].GetComponent<splineMove>().enabled = true;
                GM.Runners[i].GetComponent<Animator>().runtimeAnimatorController = anim1;
            }

        }

        if (other.gameObject.CompareTag("Trigger 7"))
        {
            for (int i = 0; i < GM.Runners7.Length; i++)
            {
                GM.Runners7[i].GetComponent<splineMove>().enabled = true;
                GM.Runners[i].GetComponent<Animator>().runtimeAnimatorController = anim1;
            }

        }

        if (other.gameObject.CompareTag("Trigger 8"))
        {
            for (int i = 0; i < GM.Runners8.Length; i++)
            {
                GM.Runners8[i].GetComponent<splineMove>().enabled = true;
                GM.Runners[i].GetComponent<Animator>().runtimeAnimatorController = anim1;
            }

        }
        if (other.gameObject.CompareTag("Trigger 9"))
        {
            for (int i = 0; i < GM.Runners9.Length; i++)
            {
                GM.Runners9[i].GetComponent<splineMove>().enabled = true;
                GM.Runners[i].GetComponent<Animator>().runtimeAnimatorController = anim1;
            }

        }
        if (other.gameObject.CompareTag("Trigger 10"))
        {
            for (int i = 0; i < GM.Runners10.Length; i++)
            {
                GM.Runners10[i].GetComponent<splineMove>().enabled = true;
                GM.Runners[i].GetComponent<Animator>().runtimeAnimatorController = anim1;
            }

        }
        if (other.gameObject.CompareTag("Trigger 11"))
        {
            for (int i = 0; i < GM.Runners11.Length; i++)
            {
                GM.Runners11[i].GetComponent<splineMove>().enabled = true;
                GM.Runners[i].GetComponent<Animator>().runtimeAnimatorController = anim1;
            }

        }
        if (other.gameObject.CompareTag("Trigger 12"))
        {
            for (int i = 0; i < GM.Runners12.Length; i++)
            {
                GM.Runners12[i].GetComponent<splineMove>().enabled = true;
                GM.Runners[i].GetComponent<Animator>().runtimeAnimatorController = anim1;
            }

        }
        if (other.gameObject.CompareTag("Solana"))
        {
            solana = solana + 0.15f;
            GM.UIM.Solana_text.text = solana.ToString();
            other.gameObject.SetActive(false);
            Solana.transform.position = other.gameObject.transform.position;
            Solana.SetActive(true);
            StartCoroutine("Dell");
        }
        if (other.gameObject.CompareTag("Etherium"))
        {
            etherium = etherium + 0.007f;
            GM.UIM.Etherium_text.text = etherium.ToString();
            other.gameObject.SetActive(false);
            Etherium.transform.position = other.gameObject.transform.position;
            Etherium.SetActive(true);
            StartCoroutine("Dell1");
        }

    }

    private IEnumerator Dell()
    {
        yield return new WaitForSeconds(2f);
        Solana.SetActive(false);
    }
    private IEnumerator Dell1()
    {
        yield return new WaitForSeconds(2f);
        Etherium.SetActive(false);
    }
}
