﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Ui_Manager : MonoBehaviour
{
    public Game_Manager GM;

    public Text distance_Cover;
    public Text Solana_text;
    public Text Etherium_text;

    public GameObject Rekt_Panel;
    public GameObject InGame_Panel;
    public GameObject Home_Panel;
    public GameObject Setting_Panel;

    public GameObject[] Panels;

    //Awake Is Called Before Start
    private void Awake()
    {
        Time.timeScale = 0f;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void OnClickhome()
    {
        Application.OpenURL("https://rektracoons.com/");
    }
    public void OnClickPlay()
    {
        Time.timeScale = 1f;
        foreach(GameObject x in Panels)
        {
            x.SetActive(false);
        }
        InGame_Panel.SetActive(true);
    }
    // Update is called once per frame
    void Update()
    {

        
    }
    public void OnClickRestart()
    {
        SceneManager.LoadScene(1);
    }
}
