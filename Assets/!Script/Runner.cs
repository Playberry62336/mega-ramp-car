﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Runner : MonoBehaviour
{
    public int Speed;
    public bool CanMove;
    public Vector3 vector3;
    // Start is called before the first frame update
    void Start()
    {
      //  transform.localRotation *= Quaternion.Euler(0, 180, 0);
    }

    // Update is called once per frame
    void Update()
    {
       if(CanMove==true)
       {
            transform.Translate(vector3 * Speed * Time.deltaTime);
       }
       
    }
}
